const RouteNavi = [
    {
        "id": 1,
        "title": "Information",
        "url": "/",
    },
    {
        "id": 2,
        "title": "Experience",
        "url": "/experience",
    },
    {
        "id": 3,
        "title": "Skills",
        "url": "/skills",
    },
    {
        "id": 1,
        "title": "Education",
        "url": "/education",
    }
];

export default RouteNavi;