import { lazy } from 'react';
import routeLink from './route-link';
import React from 'react';
import { Redirect } from 'react-router';

const exact = true;

export default [
    { exact, path: routeLink.profile.information, component: lazy(() => import('src/views/information')) },
    { exact, path: routeLink.profile.experience, component: lazy(() => import('src/views/experience')) },
    { exact, path: routeLink.profile.skills, component: lazy(() => import('src/views/skills')) },
    { exact, path: routeLink.profile.education, component: lazy(() => import('src/views/education')) },
]