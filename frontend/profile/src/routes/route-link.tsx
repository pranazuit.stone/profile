const routeLink = {
    profile: {
        information: '/',
        experience: '/experience',
        skills: '/skills',
        education: '/education',
    }
};
export default routeLink