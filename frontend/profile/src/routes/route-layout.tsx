import React, { lazy } from 'react';
import LA from 'src/layouts/MainLayout'
import routesApp from './route-layout-app';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Crypto from 'src/services/crypto';
import { IStates } from 'src/stores/root.reducer';
import routeLink from './route-link';

const exact = true;

interface IToModule {
  url: string;
}

export default [
  { path: routeLink.profile, routes: [...routesApp], layout: LA },
];
