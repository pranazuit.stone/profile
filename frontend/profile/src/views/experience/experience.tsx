import React from 'react'
import { PageContainer } from 'src/components'
import { makeStyles, Typography, Paper } from '@material-ui/core'

const useStyles = makeStyles(() => ({
    listExperience: {
        listStylePosition: 'inside',
    },
    textHeader: {
        fontSize: '20px',
        fontWeight: 'bold',
    },
    boxSpace: {
        marginBottom: '12px',
    },
}))

const ExperiencePage = () => {
    const classes = useStyles();
    const intelligent_experience = [
        'Develop Front-End Web Application with React JS, Next JS, Django Template, Wordpress, HTML, CSS and Javascript.',
        'Design API Spec and develop with Django Rest Framework and Laravel Framework.',
        'Develop Mobile Application with React Native.',
        'Develop Android Application with Java and Android Accessibility.',
        'Used Front-End Framework with Material UI, Semantic UI, Ant Design and Bootstrap.',
        'Build environment server with Docker.',
        'Create Microservices with KrakenD.',
        'Design ER Diagram.',
        'Collaborate with the app development team, including the project manager, developers, business analyst and tester.',
        'Maintenance code and debugged problems.',
        'Support production and solve production issues.',
        'Creating and executing unit tests and performing basic application testing with automate test using Robot Framework.',
    ]
    const klickerlab_experience = [
        'Develop Front-End Web Application with HTML, CSS and Javascript.',
        'Used Front-End Framework with Bootstrap.',
        'Develop Back-End with PHP.',
        'Design UX/UI Web Application.',
    ]

    return (
        <PageContainer header='Experience'>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>Software Engineer at Intelligent Bytes Co., Ltd. | Jun 2019-Feb 2023</Typography>
                <ul className={classes.listExperience}>
                    {intelligent_experience.map((item) => (
                        <li>
                            {item}
                        </li>
                    ))}
                </ul>
            </Paper>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>Internship at Klickerlab Co., Ltd. | Jan-July 2018</Typography>
                <ul className={classes.listExperience}>
                    <li>Web Application</li>
                    <li>UX/UI Design</li>
                </ul>
                <Typography><b>Project</b></Typography>
                <Typography>Enterprise Resource Planning</Typography>
                <ul className={classes.listExperience}>
                    {klickerlab_experience.map((item) => (
                        <li>
                            {item}
                        </li>
                    ))}
                </ul>
            </Paper>
        </PageContainer>
    )
}

export default ExperiencePage
