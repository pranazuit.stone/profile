import React from 'react'
import { PageContainer } from 'src/components'
import { makeStyles, Typography, Paper } from '@material-ui/core'

const useStyles = makeStyles(() => ({
    textHeader: {
        fontSize: '20px',
        fontWeight: 'bold',
    },
    boxSpace: {
        marginBottom: '12px',
    }
}))

const InformationPage = () => {
    const classes = useStyles();

    return (
        <PageContainer header='Information'>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>Personal Information</Typography>
                <Typography>Name: Pranazuit Nattabowonphal</Typography>
                <Typography>Nick name: Stone</Typography>
                <Typography>Age: 26</Typography>
                <Typography>Birth date: 24 December 1996</Typography>
                <Typography>Phone: 098-275-8178</Typography>
                <Typography>Email: pnz.stonniie@gmail.com</Typography>
                <Typography>Address: 22/69 Passorn 5 Village, Kanlapaphruek Road, Bangkhuntien, Chom thong, Bangkok 10150</Typography>
            </Paper>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>About me</Typography>
                <Typography>I am a Full Stack Developer with more than three years' experience in developing Web Application. I am passionate to learn more about the latest web technologies enhancing my skills to be more knowledgeable in the area and become an effective developer.</Typography>
            </Paper>
        </PageContainer>
    )
}

export default InformationPage
