import React from 'react'
import { PageContainer } from 'src/components'
import { Paper, makeStyles, Typography } from '@material-ui/core'

const useStyles = makeStyles(() => ({
    listExperience: {
        listStylePosition: 'inside',
    },
    textHeader: {
        fontSize: '20px',
        fontWeight: 'bold',
    },
    boxSpace: {
        marginBottom: '12px',
    }
}))

const SkillsPage = () => {
    const classes = useStyles();
    const frontend_skills = [
        'React JS', 'Django template', 'Next JS', 'React Native', 'Javascript', 'Wordpress', 'Material UI', 'Semantic UI', 'Ant Design', 'Bootstrap', 'CSS', 'HTML'
    ]
    const backend_skills = [
        'Django REST framework', 'Python', 'Laravel', 'PHP', 'Java'
    ]
    const other = [
        'Docker', 'KrakenD', 'Git', 'SQL'
    ]

    return (
        <PageContainer header='Skills'>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>Front-End</Typography>
                <ul className={classes.listExperience}>
                    {frontend_skills.map((item,key) => (
                        <li key={key}>
                            {item}
                        </li>
                    ))}
                </ul>
            </Paper>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>Back-End</Typography>
                <ul className={classes.listExperience}>
                    {backend_skills.map((item,key) => (
                        <li key={key}>
                            {item}
                        </li>
                    ))}
                </ul>
            </Paper>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>Other</Typography>
                <ul className={classes.listExperience}>
                    {other.map((item,key) => (
                        <li key={key}>
                            {item}
                        </li>
                    ))}
                </ul>
            </Paper>
        </PageContainer>
    )
}

export default SkillsPage
