import React from 'react'
import { PageContainer } from 'src/components'
import { Paper, makeStyles, Typography } from '@material-ui/core'

const useStyles = makeStyles(() => ({
    textHeader: {
        fontSize: '20px',
        fontWeight: 'bold',
    },
    boxSpace: {
        marginBottom: '12px',
    },
}))

const EducationPage = () => {
    const classes = useStyles();

    return (
        <PageContainer header='Education'>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>King Mongkut's Institute of Technology Ladkrabang | 2015-2018</Typography>
                <Typography>Computer Science</Typography>
                <Typography>B.S. (Bachelor of Science)</Typography>
            </Paper>
            <Paper className={classes.boxSpace}>
                <Typography className={classes.textHeader}>Suankularb Wittayalai School | 2011-2014</Typography>
                <Typography>Secondary School in Math&Sci</Typography>
            </Paper>
        </PageContainer>
    )
}

export default EducationPage
