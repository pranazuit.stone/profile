// เก็บข้อมูลทั่วไปที่เป็นค่าคงที่แบบไม่สามารถแก้ไขได้
export default {
    app: {
      id: 'web-back',
      name: 'Profile',
      login: true,
    },
    company: {
      name: '',
      website: '',
      trademarks: '',
    },
  };
  